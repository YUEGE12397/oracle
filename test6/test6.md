**期末项目设计报告**

| 题  目   | 基于Oracle数据库的商品销售系统的设计 |      |              |
| -------- | ------------------------------------ | ---- | ------------ |
| 课   程  | Oracle数据库应用                     |      |              |
| 学  院   | 计算机学院                           |      |              |
| 专  业   | 软件工程                             | 年级 | 2020级       |
| 学生姓名 | 余悦歌                               | 学号 | 202010414325 |
| 指导教师 | 赵卫东                               | 职称 | 教授         |

 

| ***\*评分项\**** | ***\*评分标准\****           | ***\*满分\**** | ***\*得分\**** |
| ---------------- | ---------------------------- | -------------- | -------------- |
| 文档整体         | 文档内容详实、规范，美观大方 | 10             |                |
| 表设计           | 表，表空间设计合理，数据合理 | 20             |                |
| 用户管理         | 权限及用户分配方案设计正确   | 20             |                |
| PL/SQL设计       | 存储过程和函数设计正确       | 30             |                |
| 备份方案         | 备份方案设计正确             | 20             |                |
| **得分合计**     |                              |                |                |

2023 年  5 月  26 日

 

 

 

# **（一）概要设计**

## **一、绪论**

近年来，随着互联网的快速发展，电商行业也迎来了前所未有的机遇。越来越多的企业开始尝试通过电商平台进行线上销售，以拓展业务和增加销售渠道。然而，由于缺乏有效的销售管理系统，许多企业在销售过程中遇到了各种问题，如商品管理效率低下、订单处理困难、客户信息不准确等。

为了解决这些问题，我们开发了商品销售系统。该系统可以帮助企业实现线上销售、营销和管理，提高销售效率和管理水平，从而提升企业的竞争力。同时，该系统还提供了丰富的数据分析功能，帮助企业更好地了解市场和客户需求，为企业的决策提供数据支持。。

## **二、数据库部署模式**

越来越多的人选择去电商购物、庞大商品数据为电影院带来巨大利润的同时,也带来了信息系统风险的相对集中，这使得电商信息系统连续运行的要求也越来越高。加强信息系统灾备体系建设，保障业务连续运行，已经成为影响影院竞争能力的一个重要因素。对RTO=0/RPO=0的系统决定数据库采用RAC+DataDataGuard模式。根据RAC+DataDataGuard模式的特点，有如下要求: .1 主机与备机在物理.上要分开。为了实现容灾的特性，需要在物理。上分割主机和备机。. .2 进行合理 的设计，充分实现DATAGUARD的功能。 注: RTO ( RecoveryTime object): 恢复时间目标，灾难发生后信息系统从停顿到必须恢复的时间要求。 RPO (Recovery Point Object): 恢复点目标，指一个过去的时间点，当灾难或紧急事件发生时，数据可以恢复到的时间点。

## **三、Oracle数据库**

Oracle数据库是美国甲骨文公司（Oracle Corporation）推出的关系型数据库管理系统。它是全球最大的信息管理软件及服务供应商，成立于1977年，总部位于美国加州Redwood shore。Oracle数据库产品为财富排行榜上的前1000家公司所采用，许多大型网站也选用了Oracle系统，是世界最好的数据库产品。



 

 

 

# （二）**详细设计**

创建表空间和用户：

CREATE TABLESPACE system_tablespace

 DATAFILE 'system_tablespace.dbf'

 SIZE 100M

 AUTOEXTEND ON

 NEXT 10M

 MAXSIZE 500M;

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps1.jpg) 

CREATE TEMPORARY TABLESPACE user_tablespace

 TEMPFILE 'user_tablespace.dbf'

 SIZE 100M

 AUTOEXTEND ON

 NEXT 10M

 MAXSIZE 500M;

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps2.jpg) 

创建用户：

-- 创建管理员用户

CREATE USER admin_user

 IDENTIFIED BY 12345

 DEFAULT TABLESPACE system_tablespace

 TEMPORARY TABLESPACE user_tablespace;

-- 授权

GRANT DBA TO ADMIN_USER;

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps3.jpg) 

创建普通用户：

CREATE USER app_user

 IDENTIFIED BY 12345

 DEFAULT TABLESPACE system_tablespace

 TEMPORARY TABLESPACE user_tablespace;

 

GRANT CREATE SESSION, CREATE TABLE, CREATE VIEW TO app_user;

 

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps4.jpg) 

创建表：

 

create table customer(

customerid  varchar(10) not null primary key,

customerName  varchar(10),

customerSex varchar(2),

customerPhone varchar(50)  not null,

customeraddress varchar(50) not null

);

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps5.jpg) 

create table good(

goodId varchar(10)  not null primary key,

goodName varchar(100) not null,

goodPrice float not null,

goodType varchar(10),

goodStock int,

SafetyStock int

);

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps6.jpg) 

create table orders(

OrderId  int  not null  primary key,

customerid  varchar(10)  not null,

OrderDate  date  not null,

FOREIGN KEY(customerid) REFERENCES customer(customerid)

);

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps7.jpg) 

create table orderdetail(

OrderId  int  not null,

goodId   varchar(10) not null,

OrderNum  int  not null,

OrderPrice  float  not null,

primary key(OrderId,goodId),

FOREIGN KEY(OrderId) REFERENCES orders(OrderId),

FOREIGN KEY(goodId) REFERENCES good(goodId)

);

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps8.jpg) 

模拟插入数据：

-- 创建一个存储过程来插入顾客数据。

CREATE OR REPLACE PROCEDURE insert_customer_data AS

BEGIN

FOR i IN 1..100000 LOOP

INSERT INTO customer (customerid, customerName, customerSex, customerPhone, customeraddress)

VALUES (

'C'||LPAD(i, 6, '0'),

'Customer'||LPAD(i, 3, '0'),

CASE WHEN MOD(i, 2) = 0 THEN '女' ELSE '男' END,

'188123456'||LPAD(i, 2, '0'),

'地址'||LPAD(i, 3, '0')

);

END LOOP;

COMMIT;

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps9.jpg) 

-- 创建一个存储过程来插入10万条商品信息

CREATE OR REPLACE PROCEDURE insert_good_data AS

BEGIN

FOR i IN 1..100000 LOOP

INSERT INTO good (goodId, goodName, goodPrice, goodType, goodStock, SafetyStock)

VALUES (

'G'||LPAD(i, 6, '0'),

'商品'||LPAD(i, 3, '0'),

100 + i,

'分类'||LPAD(i, 3, '0'),

i,

i/2

);

END LOOP;

COMMIT;

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps10.jpg) 

-- 创建一个存储过程来插入订单信息。

CREATE OR REPLACE PROCEDURE insert_order_data AS

BEGIN

FOR i IN 1..100000 LOOP

INSERT INTO orders (OrderId, customerid, OrderDate)

VALUES (

i,

'C'||LPAD(i, 6, '0'),

TO_DATE('2022-01-01', 'YYYY-MM-DD') + MOD(i, 365)

);

END LOOP;

COMMIT;

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps11.jpg) 

-- 创建一个存储过程来插入订单明细信息。

CREATE OR REPLACE PROCEDURE insert_order_detail_data AS

BEGIN

FOR i IN 1..100000 LOOP

INSERT INTO orderdetail (OrderId, goodId, OrderNum, OrderPrice)

VALUES (

i,

'G'||LPAD(MOD(i, 12) + 1, 3, '0'),

1,

100 + MOD(i, 900)

);

END LOOP;

COMMIT;

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps12.jpg) 

BEGIN

 insert_customer_data;

 insert_good_data;

 insert_order_data;

 insert_order_detail_data;

END;

/

select count(*) from customer;

select count(*) from good;

select count(*) from orders;

select count(*) from orderdetail;

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps13.jpg) 

 

创建程序包

 

CREATE OR REPLACE PACKAGE my_package AS

 -- 存储过程：根据顾客ID查询订单总金额

 PROCEDURE get_order_total_amount(

  p_customer_id IN VARCHAR2,

  p_total_amount OUT NUMBER

 );

 

 -- 函数：计算商品销售额

 FUNCTION calculate_product_sales(

  p_product_id IN VARCHAR2

 ) RETURN NUMBER;

 

END my_package;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps14.jpg) 

CREATE OR REPLACE PACKAGE BODY my_package AS

 -- 存储过程：根据顾客ID查询订单总金额

 PROCEDURE get_order_total_amount(

  p_customer_id IN VARCHAR2,

  p_total_amount OUT NUMBER

 ) IS

  v_total_amount NUMBER;

 BEGIN

  -- 查询订单总金额

  SELECT SUM(od.OrderNum * od.OrderPrice)

  INTO v_total_amount

  FROM orders o

  JOIN orderdetail od ON o.OrderId = od.OrderId

  WHERE o.customerid = p_customer_id;

  

  -- 将结果赋值给 OUT 参数

  p_total_amount := v_total_amount;

 END;

 

 -- 函数：计算商品销售额

 FUNCTION calculate_product_sales(

  p_product_id IN VARCHAR2

 ) RETURN NUMBER IS

  v_total_sales NUMBER;

 BEGIN

  -- 查询商品销售额

  SELECT SUM(od.OrderNum * od.OrderPrice)

  INTO v_total_sales

  FROM orderdetail od

  WHERE od.goodId = p_product_id;

  

  -- 返回计算结果

  RETURN v_total_sales;

 END;

 

END my_package;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps15.jpg) 

set serveroutput on

-- 调用存储过程：根据顾客ID查询订单总金额

DECLARE

 v_customer_id VARCHAR2(10) := 'C000001';  -- 顾客ID

 v_total_amount NUMBER;  -- 总金额

BEGIN

 my_package.get_order_total_amount(v_customer_id, v_total_amount);

 

 -- 输出结果

 DBMS_OUTPUT.PUT_LINE('顾客 ' || v_customer_id || ' 的订单总金额为：' || v_total_amount);

END;

/

 

-- 调用函数：计算商品销售额

DECLARE

 v_product_id VARCHAR2(10) := 'G000001';  -- 商品ID

 v_total_sales NUMBER;  -- 销售额

BEGIN

 v_total_sales := my_package.calculate_product_sales(v_product_id);

 

 -- 输出结果

 DBMS_OUTPUT.PUT_LINE('商品 ' || v_product_id || ' 的销售额为：' || v_total_sales);

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml9392\wps16.jpg) 

 

备份计划设置：

定期完全备份（Full Backup）：定期对整个数据库进行完全备份。完全备份将所有数据和对象都备份到一个独立的文件中，提供了最全面的恢复能力。可以使用Oracle提供的工具如RMAN（Recovery Manager）来执行完全备份。

 

日志备份（Archive Log Backup）：启用归档日志模式，并定期备份归档日志。归档日志记录了数据库中的所有变更操作，通过备份归档日志，可以将数据库恢复到备份完成后的状态。定期备份归档日志可以确保在故障发生时，可以应用最新的变更操作，减少数据丢失。
