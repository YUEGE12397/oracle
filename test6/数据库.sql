CREATE TABLESPACE system_tablespace
  DATAFILE 'system_tablespace.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 10M
  MAXSIZE 500M;

CREATE TEMPORARY TABLESPACE user_tablespace
  TEMPFILE 'user_tablespace.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 10M
  MAXSIZE 500M;




-- 创建管理员用户
CREATE USER admin_user
  IDENTIFIED BY 12345
  DEFAULT TABLESPACE system_tablespace
  TEMPORARY TABLESPACE user_tablespace;
-- 授权
GRANT DBA TO ADMIN_USER;

CREATE USER app_user
  IDENTIFIED BY 12345
  DEFAULT TABLESPACE system_tablespace
  TEMPORARY TABLESPACE user_tablespace;

GRANT CREATE SESSION, CREATE TABLE, CREATE VIEW TO app_user;

create table customer(
customerid  varchar(20) not null primary key,
customerName  varchar(20),
customerSex varchar(2),
customerPhone varchar(50)  not null,
customeraddress varchar(50) not null
);

create table good(
goodId varchar(20)  not null primary key,
goodName varchar(100) not null,
goodPrice float not null,
goodType varchar(20),
goodStock int,
SafetyStock int
);

create table orders(
OrderId   int  not null  primary key,
customerid  varchar(20)  not null,
OrderDate  date  not null,
FOREIGN KEY(customerid) REFERENCES customer(customerid)
);

create table orderdetail(
OrderId  int  not null,
goodId    varchar(20) not null,
OrderNum  int   not null,
OrderPrice  float  not null,
primary key(OrderId,goodId),
FOREIGN KEY(OrderId) REFERENCES orders(OrderId),
FOREIGN KEY(goodId) REFERENCES good(goodId)
);

-- 创建一个存储过程来插入顾客数据。
CREATE OR REPLACE PROCEDURE insert_customer_data AS
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO customer (customerid, customerName, customerSex, customerPhone, customeraddress)
VALUES (
'C'||LPAD(i, 6, '0'),
'Customer'||LPAD(i, 3, '0'),
CASE WHEN MOD(i, 2) = 0 THEN '女' ELSE '男' END,
'188123456'||LPAD(i, 2, '0'),
'地址'||LPAD(i, 3, '0')
);
END LOOP;
COMMIT;
END;
/

-- 创建一个存储过程来插入10万条商品信息
CREATE OR REPLACE PROCEDURE insert_good_data AS
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO good (goodId, goodName, goodPrice, goodType, goodStock, SafetyStock)
VALUES (
'G'||LPAD(i, 6, '0'),
'商品'||LPAD(i, 3, '0'),
100 + i,
'分类'||LPAD(i, 3, '0'),
i,
i/2
);
END LOOP;
COMMIT;
END;
/

-- 创建一个存储过程来插入订单信息。
CREATE OR REPLACE PROCEDURE insert_order_data AS
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO orders (OrderId, customerid, OrderDate)
VALUES (
i,
'C'||LPAD(i, 6, '0'),
TO_DATE('2022-01-01', 'YYYY-MM-DD') + MOD(i, 365)
);
END LOOP;
COMMIT;
END;
/

-- 创建一个存储过程来插入订单明细信息。
CREATE OR REPLACE PROCEDURE insert_order_detail_data AS
BEGIN
FOR i IN 1..100000 LOOP
INSERT INTO orderdetail (OrderId, goodId, OrderNum, OrderPrice)
VALUES (
i,
'G'||LPAD(i, 6, '0'),
1,
100 + MOD(i, 900)
);
END LOOP;
COMMIT;
END;
/

BEGIN
  insert_customer_data;
  insert_good_data;
  insert_order_data;
  insert_order_detail_data;
END;
/
select count(*) from customer;
select count(*) from good;
select count(*) from orders;
select count(*) from orderdetail;



CREATE OR REPLACE PACKAGE my_package AS
  -- 存储过程：根据顾客ID查询订单总金额
  PROCEDURE get_order_total_amount(
    p_customer_id IN VARCHAR2,
    p_total_amount OUT NUMBER
  );
  
  -- 函数：计算商品销售额
  FUNCTION calculate_product_sales(
    p_product_id IN VARCHAR2
  ) RETURN NUMBER;
  
END my_package;
/

CREATE OR REPLACE PACKAGE BODY my_package AS
  -- 存储过程：根据顾客ID查询订单总金额
  PROCEDURE get_order_total_amount(
    p_customer_id IN VARCHAR2,
    p_total_amount OUT NUMBER
  ) IS
    v_total_amount NUMBER;
  BEGIN
    -- 查询订单总金额
    SELECT SUM(od.OrderNum * od.OrderPrice)
    INTO v_total_amount
    FROM orders o
    JOIN orderdetail od ON o.OrderId = od.OrderId
    WHERE o.customerid = p_customer_id;
    
    -- 将结果赋值给 OUT 参数
    p_total_amount := v_total_amount;
  END;
  
  -- 函数：计算商品销售额
  FUNCTION calculate_product_sales(
    p_product_id IN VARCHAR2
  ) RETURN NUMBER IS
    v_total_sales NUMBER;
  BEGIN
    -- 查询商品销售额
    SELECT SUM(od.OrderNum * od.OrderPrice)
    INTO v_total_sales
    FROM orderdetail od
    WHERE od.goodId = p_product_id;
    
    -- 返回计算结果
    RETURN v_total_sales;
  END;
  
END my_package;
/

set serveroutput on
-- 调用存储过程：根据顾客ID查询订单总金额
DECLARE
  v_customer_id VARCHAR2(10) := 'C000001';  -- 顾客ID
  v_total_amount NUMBER;  -- 总金额
BEGIN
  my_package.get_order_total_amount(v_customer_id, v_total_amount);
  
  -- 输出结果
  DBMS_OUTPUT.PUT_LINE('顾客 ' || v_customer_id || ' 的订单总金额为：' || v_total_amount);
END;
/

-- 调用函数：计算商品销售额
DECLARE
  v_product_id VARCHAR2(10) := 'G000001';  -- 商品ID
  v_total_sales NUMBER;  -- 销售额
BEGIN
  v_total_sales := my_package.calculate_product_sales(v_product_id);
  
  -- 输出结果
  DBMS_OUTPUT.PUT_LINE('商品 ' || v_product_id || ' 的销售额为：' || v_total_sales);
END;
/
